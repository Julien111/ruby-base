for num in 1..10
    puts num
end

# autre test on exclus 10

for num in 1...10
    puts num
end

# autre test 
adeviner = 6

for num in 1..3
    puts("Entrez un chiffre entre 1 et 20 : ")

    if (3- num + 1) == 1
        puts("Dernière tentatives")
    else
        puts("Vous n'avez plus que #{3 - num + 1} possibilités ! ")
    end
    
    chiffre = gets.chomp.to_i

    if(chiffre < adeviner)
        puts("Le chiffre est trop bas")            
    elsif (chiffre > adeviner)
        puts("Le chiffre est trop grand")
    else
        puts "Vous avez réussi."
        break;
    end
      
end
