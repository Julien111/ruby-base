=begin
def majuscule(mot)   
    if mot.respond_to?(:to_s)
        mot.to_s.upcase        
    else
        puts "erreur"
    end
end

puts majuscule("test")
puts majuscule(56)
puts majuscule(156.12)
=end

# test des blocs
=begin
def demo
    puts "Bonjour !!!!"
    yield("Jules")
    puts "Ciao"
end

demo { |nom| puts "Comment ça va #{nom} ?"}
=end

# test des blocs
=begin
notesEleves = {'Jean' => 11, 'Claire' => 15, 'Bob' => 9}

def alamoyenne(eleves)
    eleves.each do |eleve, note|
        if note >= 10
            yield(eleve)
        end
    end
end

alamoyenne(notesEleves) do |eleve|
    puts "#{eleve} a la moyenne."
end
=end

# Mettre au carré
a = [2,4,6,40]
a.map! { |n| n**2}
puts a.inspect

#test sur les blocs

# test pour voir la classe d'origine
def block_class(&block)
    puts block.class
end

# block_class {}

# les procs

tab2 = [15, 20, 30, 40]
carre = Proc.new { |n| n**2}
tab2.map(&carre)

# test two proc

notesEleves = {'Jean' => 11, 'Claire' => 15, 'Bob' => 9}

def alamoyenne(eleves, messageMoy, pasLaMoy)
    eleves.each do |eleve, note|
        if note >= 10
            messageMoy.call(eleve, 'test')
        else
            pasLaMoy.call(eleve)
        end
    end
end

#messageMoy = Proc.new{ |eleve| puts "#{eleve} a la moyenne." }
messageMoy = Proc.new { |eleve| puts "#{eleve} a la moyenne." }
#pasLaMoy = Proc.new{ |eleve| puts "#{eleve} n'a pas la moyenne." }
pasLaMoy = lambda { |eleve| puts "#{eleve} n'a pas la moyenne." }

alamoyenne(notesEleves, messageMoy, pasLaMoy)


# test three lambda


