# On va avoir une variable avec un texte
# On supprime tous les éléments de ponctuation

texte = "Pour les vacances, j’ai loué une voiture et je suis parti dans le Morbihan. C’est dans l’ouest de la France, les villes les plus connues sont Vannes ou encore Carnac. C’est une région où il y a beaucoup de vestiges celtiques : les menhirs, les dolmens et les alignements de pierres. C’est très connu et très impressionnant."
newTexte = texte.gsub(/[^A-Z’a-z0-9éèàçùêûîï\s]/i, '')
newTexte.gsub!(/’/, ' ')

# mettre tous les mots en minuscules
newTexte.downcase!
# puts newTexte
# tableau
tabWord = newTexte.split 
wordsRef = {}
# On va récupérer les mots un à un 
# les stocker et on va devoir compter le nombre de fois qu'ils sont dans ce texte 

for index in 0...tabWord.length
    count = 0
    word = tabWord[index];
    for num in 0...tabWord.length
        if word == tabWord[num]
            count += 1;
        end
    end
    wordsRef[word] = count;
end

# sort by
# stocker le résultat dans une variable
sortArr = wordsRef.sort_by do |mot, compteur|
    compteur
end
puts sortArr.reverse!

# puts wordsRef

puts "Tapez un mot pour voir combien de fois il est présent dans le texte : "
motFin = gets.downcase.chomp
puts wordsRef[motFin]
# prompt pour vérifier
