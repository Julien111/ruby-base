# Les tableaux

tab = ["Marc", "Jules", "Fred", "Laurent", "Diego"]

puts tab.length  
puts tab.last 

# ajouter des éléments
# certaines méthodes ressemblent au JavaScript

tab.push("Margot")
tab += ["Jean"]
tab.push("Gregoire", "Dan")
tab << "André"

puts tab.length   
puts tab.last 

# exemple tableau imbriqué

testTab = [[1,2], "John", 45, true]
puts testTab[3]
puts testTab[2]
puts testTab[1]
puts testTab[0][1]

# hash

marc = {'age' => 31, 'adulte' => true, 'nom' => 'Delgado'}
puts marc['age']
puts marc['adulte']

# les symboles

kevin = {note: 18, age: 17, taille: 185, nom: "Marcos"}
puts kevin[:note]
puts kevin[:nom]