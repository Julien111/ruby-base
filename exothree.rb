puts("Entrez un mot : ")
mot = gets.chomp.downcase

inverse = mot.reverse

if mot.eql?(inverse)
    puts("Le mot est un palindrome")
else
    puts("Le mot n'est pas un palindrome")
end