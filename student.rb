module Notable

    MOYENNE = 10

    def self.included(base)
        puts base
    end

    public
    def addNote(note)
        raise ArgumentError, "La note n'est pas un entier" if !note.respond_to? :to_i
        @notes.push(note)              
    end

    def is_Moyenne?()
        total = 0;
        for num in @notes
            total += num;           
        end
        
        puts result = total / @notes.length

        if result < MOYENNE
           return false
        else
           return true 
        end
    end

    def theMoyenne 
        total = 0;
        for num in @notes
            total += num;           
        end        
        return result = total / @notes.length
    end

end

class Student

    MOYENNE = 10
    
    attr_accessor :nom, :notes   

    include Notable

    def initialize(nom)
        @nom = nom
        @notes = []
    end

    def afficheNom 
        puts "Je suis #{nom}"
    end
    
end

jean = Student.new('Jean');
jean.addNote(8);
jean.addNote(5);
begin    
    jean.addNote([12,14]);
rescue Exception 
    puts "La note n'a pas pu être ajoutée"
end
jean.addNote(8);
jean.addNote(11);
# puts jean.notes
# puts jean.is_Moyenne?();
# jean.theMoyenne

class Delegue < Student

    def afficheMoy
        puts MOYENNE
    end

    def theMoyenne 
        theMoyenne = super
        return theMoyenne + 1        
    end

end

class Professeur 
    include Notable

    attr_accessor :nom, :notes 

    def initialize(nom)
        @nom = nom
        @notes = []
    end
end

d = Delegue.new("Marc");
puts d.nom
d.addNote(14)
d.addNote(12)
d.addNote(12)
puts d.theMoyenne
puts d.notes
puts d.class

prof = Professeur.new("Delgado")
prof.addNote(['15'])
puts prof.theMoyenne