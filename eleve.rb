class Eleve

    attr_accessor :nom, :age

    # MAJORITE = 18;
    @@majorite = 18;

    @@verif = "ok"

    def initialize(nom, age)
      @nom = nom
      @age = age
    end

    def bonjour
        puts "Bonjour je suis #{@nom} et j'ai #{@age}"
        if isMajeur?
            puts "Je suis majeur"
        else
            puts "Je ne suis pas majeur"
        end
    end

    def isMajeur?
        if @age >= 18 
            puts true
        else
            puts false
        end      
    end

    def self.modif(majo)
        @@majorite = majo
        puts @@majorite
    end

    def self.calcul(num)
        if num.is_a?(Integer)
            puts (num.to_i + 1)
            puts @@verif
        end        
    end
    # test du mot privé
    # def demoPublic
    #     puts demo
    # end

    # private

    # def demo
    #     puts "test"
    # end
    
end


#créer des objets

eleve0 = Eleve.new("Vince", 17)
eleve0.age = 18
Eleve.modif(21)
# eleve0.demoPublic
puts eleve0.age
eleve1 = Eleve.new("Jean", 16)
# eleve1.bonjour
eleve2 = Eleve.new("Marc", 17)
Eleve.calcul(15)
# eleve2.bonjour
# eleve2.isMajeur?
# eleve0.isMajeur?


# rappel
=begin
@ = variable d'instance
@@ = variable de class
$ = global
MAJUSCULE = constante
=end
