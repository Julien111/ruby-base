loop do
    puts "Hello"
    break;
end

3.times do |num|
    puts(num)
end

# block avec each
eleves = ["Jean", "John", "Marcus"]

eleves.each do |nom|
    puts "Les noms sont : #{nom}"
end

# les hashs

marc={nom: "Delgado", age: 20, pays: "France"}
marc.each do |cle, valeur|
    puts "#{cle} : #{valeur}"
end