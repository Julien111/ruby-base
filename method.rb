# création de méthodes (fonction)

def transformSecToMin(seconds)
    seconds = seconds.to_i
    return seconds/60
end

# test

def convert_seconds(min)
    min = min.to_i
    min*60
end

# puts transformSecToMin(400)
# puts transformSecToMin(1600)
# puts transformSecToMin(1200)
# puts convert_seconds("60")

# test des méthodes

def salutation(nom, prefix = "Bonjour")
    puts "#{prefix} #{nom}"
end

# salutation("Jean")
# salutation("John")
# salutation("John", "Salut")

#gestion des listes

def reorganierListe(is_true, *nom)
     # on convertit en string
     newArrNames = []
     noms = nom.each do |nom|
         newArrNames.push(nom.to_s)            
     end

    # on test le code en faisant un premier tri
    newArrNames = newArrNames.sort_by do |word|
        word.downcase
    end
    # réponse
    if(is_true)       
        puts newArrNames
    else        
        puts newArrNames.reverse
    end
end

reorganierListe(true, 'Tall','Marc', 'Jean', 'Dan', 'David', 'Fabrice', 'Jules', 'Grégoire', :Manon, :Anna)