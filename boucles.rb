# while

index = 0;

# while index < 10
#     index = index + 1
#     puts index
# end

# exercice
adeviner = 4

# boucle
puts("Entrez un chiffre entre 1 et 20 : ")
chiffre = gets.chomp.to_i

while adeviner != chiffre
    if(chiffre < adeviner)
        puts("Le chiffre est trop bas")
    end
    if(chiffre > adeviner)
        puts("Le chiffre est trop grand")
    end
    puts("essayez encore : ")
    chiffre = gets.chomp.to_i
    # puts chiffre
    # puts adeviner    
end
puts "Vous avez réussi."