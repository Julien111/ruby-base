def premierLambda 
    a = lambda { return "salut" }
    a.call
    return "ciao"
end

def a_proc 
    a = Proc.new{ return "salut"}
    a.call  
    return "au revoir"
end

puts premierLambda
puts a_proc

# test lambda

notesEleves = {'Jean' => 11, 'Claire' => 15, 'Bob' => 9}

def alamoyenne(eleves, moyenneproc)
    eleves.each do |nom, note|       
        if note >= 10
            moyenneproc.call(nom)
        end
    end    
end

afficheMoy = -> (eleve) { puts "#{eleve} a la moyenne"}
alamoyenne(notesEleves, afficheMoy)

# symbole

